import java.io.*;
import java.util.*;
import java.text.*;

public class Aluguer implements Serializable{
    private int flag;
    private String user;
    private Veiculos veiculo;
    private GregorianCalendar data;
    private Coordenadas ini;
    private Coordenadas fim;
    private double distancia;
    private double preco;
    private int tempo;
    
    public Aluguer(String cliente,Veiculos veiculo,GregorianCalendar d,Coordenadas ini, Coordenadas fim, double distancia, double preco, int tempo){
        this.flag = 0;
        this.user = cliente;
        this.veiculo = veiculo;
        this.data = d;
        this.ini = ini;
        this.fim = fim;
        this.distancia = distancia;
        this.preco = preco;
        this.tempo = tempo;
    }
    
    public Aluguer(Aluguer a){
        this.flag = a.getFlag();
        this.user = a.getCliente();
        this.veiculo = a.getVeiculo();
        this.data = a.getData();
        this.ini = a.getInic();
        this.fim = a.getFim();
        this.distancia = a.getDist();
        this.preco = a.getPreco();
        this.tempo = a.getTempo();
    }
    
    public int getFlag(){return this.flag;}
    public String getCliente(){return this.user;}
    public Veiculos getVeiculo(){return this.veiculo;}
    public GregorianCalendar getData(){return (GregorianCalendar) this.data.clone();}
    public Coordenadas getInic(){return this.ini;}
    public Coordenadas getFim(){return this.fim;}
    public double getDist(){return this.distancia;}
    public double getPreco(){return this.preco;}
    public int getTempo(){return this.tempo;}
    
    public void setFlag(int f){this.flag = f;}
    public void setCliente(String cli){this.user = cli;}
    public void setVeiculo(Veiculos vei){this.veiculo = vei.clone();}
    public void setData(GregorianCalendar dat){this.data = dat;}
    public void setInic(Coordenadas i){this.ini = i.clone();}
    public void setFim(Coordenadas f){this.fim = f.clone();}
    public void setDist(double dist){this.distancia = dist;}
    public void setPreco(double prec){this.preco = prec;}
    public void setTempo(int temp){this.tempo = temp;}
    
    public Aluguer clone(){return new Aluguer(this);}
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass()))return false;
        Aluguer a = (Aluguer) o;
        return (this.user.equals(a.getCliente()) && this.veiculo.equals(a.getVeiculo()) && this.data.equals(a.getData()) && this.ini.equals(a.getInic()) && this.fim.equals(a.getFim()));
    }
    public String toString(){
        StringBuilder s=new StringBuilder();
        s.append("User: ");
        s.append(this.user);
        s.append(System.lineSeparator());
        s.append("Data em que da viagem foi realizada : ");
        SimpleDateFormat dataRegAux= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
        String dataRegAuxS=dataRegAux.format(data.getTime());
        s.append(dataRegAuxS);
        s.append(System.lineSeparator());
        s.append("Veiculo utilizado: ");
        s.append(this.veiculo.getMatricula());
        s.append(System.lineSeparator());
        s.append("Coordenadas iniciais: ");
        s.append(this.ini);
        s.append(System.lineSeparator());
        s.append("Coordenadas finais: ");
        s.append(this.fim);
        s.append(System.lineSeparator());
        s.append("Distancia percorrida : ");
        s.append(this.distancia);
        s.append(System.lineSeparator());
        s.append("Preco pago: ");
        s.append(this.preco);
        s.append(System.lineSeparator());
        s.append("Tempo da viagem: ");
        s.append(this.preco);
        s.append(System.lineSeparator());
        return s.toString();
    }
} 


import java.io.*;
import java.util.*;

public class CarroHibrido extends Veiculos{
    
    public CarroHibrido(String descricao, boolean estado,String matricula,int velocidade, double preco, int consumo, int classificacao, int autonomia, Coordenadas local){
        super(descricao,estado,matricula,velocidade,preco,consumo,classificacao,autonomia,local);
    }
    
    public CarroHibrido(CarroHibrido ch){
        super(ch); 
    }
    
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass()))return false;
        CarroHibrido a = (CarroHibrido) o;
        return (this.getMatricula().equals(a.getMatricula()));
    }
    
    public CarroHibrido clone(){
        return new CarroHibrido(this);
    }
}

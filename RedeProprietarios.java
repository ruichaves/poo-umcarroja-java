import java.util.*;
import java.io.*;

public class RedeProprietarios{
    TreeMap<String,Proprietario> rede;
    
    public RedeProprietarios(){
        rede = new TreeMap<String,Proprietario>();
    }
    
    public RedeProprietarios(RedeProprietarios rp){
        this.rede = rp.getRede();
    }
    
    public TreeMap<String,Proprietario> getRede(){
        TreeMap<String,Proprietario> aux = new TreeMap<String,Proprietario>();
        for(Proprietario p:rede.values()){
            aux.put(p.getEmail(),p.clone());
        }
        return aux;
    }
    
    public void setRede(TreeMap<String,Proprietario> rp){
        TreeMap<String,Proprietario> aux = new TreeMap<String,Proprietario>();
        for(Proprietario p:rp.values()){
            aux.put(p.getEmail(),p.clone());
        }
        rede=aux;
    }
    
    public void addProprietario(Proprietario p){
        rede.put(p.getEmail(),p.clone());
    }
    
    public void removeProprietario (Proprietario p){
        rede.remove(p.getEmail());
    }
    
    public int login(String email,String password){
        if (rede.get(email).getPass().equals(password)) return 1;
        else return 0;
    }
    
    public Proprietario getProprietario(String p){
        return rede.get(p).clone();
    }
    
    public void trocaProprietario(Proprietario p){
        rede.put(p.getEmail(),p.clone());
    }
    
    public RedeProprietarios clone(){
        return new RedeProprietarios(this);
    }
    
    public boolean equals(Object o){
        if (o == this) return true;
        if ((o==null) || (this.getClass() != o.getClass())) return false;
        RedeProprietarios aux = (RedeProprietarios) o;
        int conta = 0;
        for (Proprietario p: rede.values()){
            if (p.equals(aux.getProprietario(p.getEmail()))) conta++;
        }
        if (conta == this.rede.size()) return true;
        return false;
    }
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        for(Proprietario p: this.rede.values()) s.append(p.toString());
        s.append(System.lineSeparator());
        return s.toString();
    }
}

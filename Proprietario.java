import java.io.*;
import java.util.*;
import java.text.*;

public class Proprietario{
    private String email;
    private String nome;
    private String password;
    private String morada;
    private GregorianCalendar dataNascimento;
    private int classificacao;
    private HashMap<String,Veiculos> listaVeiculos;
    private HashMap<String,Cliente> listapedidos;
    private TreeSet<Aluguer> historico;
    
    public Proprietario(String email, String nome, String pass, String morada, GregorianCalendar data, int clas){
        this.email = email;
        this.nome = nome;
        this.password = pass;
        this.morada = morada;
        this.dataNascimento = data;
        this.classificacao = clas;
        this.listaVeiculos = new HashMap<String,Veiculos>();
        this.listapedidos = new HashMap<String,Cliente>();
        this.historico = new TreeSet<Aluguer>(new AluguerComparator());
    }
    
    public Proprietario(Proprietario p){
        this.email = p.getEmail();
        this.nome = p.getNome();
        this.password = p.getPass();
        this.morada = p.getMorada();
        this.dataNascimento = p.getData();
        this.classificacao = p.getClas();
        this.listaVeiculos = p.getVeiculos();
        this.listapedidos = p.getPedidos();
        this.historico = p.getHistorico();
    }
    
    public String getEmail(){return this.email;}
    public String getNome(){return this.nome;}
    public String getPass(){return this.password;}
    public String getMorada(){return this.morada;}
    public GregorianCalendar getData(){ return (GregorianCalendar) this.dataNascimento.clone();}
    public int getClas(){return this.classificacao;}
    public HashMap<String,Veiculos> getVeiculos(){
       HashMap<String,Veiculos> aux = new HashMap<String,Veiculos>();
       for(Map.Entry<String,Veiculos> entry : this.listaVeiculos.entrySet()){
            String m = entry.getKey();
            Veiculos v = entry.getValue();
            aux.put(m,v.clone());
       }
       return aux;
    }
    public HashMap<String,Cliente> getPedidos(){
       HashMap<String,Cliente> aux = new HashMap<String,Cliente>();
       for(Map.Entry<String,Cliente> entry : this.listapedidos.entrySet()){
            String m = entry.getKey();
            Cliente c = entry.getValue();
            aux.put(m,c.clone());
       }
       return aux;
    }
    public TreeSet<Aluguer> getHistorico(){
        TreeSet<Aluguer> hist = new TreeSet<Aluguer>(new AluguerComparator());
        Iterator<Aluguer> it = this.historico.iterator();
        while(it.hasNext()){hist.add(it.next());}
        return hist;
    }
    
    public void setEmail(String email){this.email = email;}
    public void setNome(String nome){this.nome = nome;}
    public void setPass(String pass){this.password = pass;}
    public void setMorada(String morada){this.morada = morada;}
    public void setData(GregorianCalendar data){this.dataNascimento = data;}
    public void setClas(int clas){this.classificacao = clas;}
    public void setVeiculos(HashMap<String,Veiculos> v){
        this.listaVeiculos=v;
    }
    public void setPedidos(HashMap<String,Cliente> p){
        this.listapedidos = p;
    }
    public void setHistorico(TreeSet<Aluguer> h){
       TreeSet<Aluguer> aux = new TreeSet<Aluguer>(new AluguerComparator());
       Iterator<Aluguer> it = h.iterator();
       while(it.hasNext()){
            aux.add(it.next());
       }
       this.historico=aux;
    }
    
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass()))return false;
        Proprietario a = (Proprietario) o;
        return (this.email.equals(a.getEmail()));
    }
    public Proprietario clone(){return new Proprietario(this);}
    public String toString(){  
        StringBuilder s = new StringBuilder(); 
        s.append("Email: ");
        s.append(email);
        s.append(System.lineSeparator());
        s.append("Nome: ");
        s.append(nome);
        s.append(System.lineSeparator());
        s.append("Data de nascimento: ");
        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(dataNascimento.getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        s.append("Classificacao: ");
        s.append(classificacao);
        s.append(System.lineSeparator());
        return s.toString();
    }
    
    public void addVeiculo (Veiculos v){
        this.listaVeiculos.put(v.getMatricula(),v.clone());
    }
    
    public void remVeiculo(Veiculos v){
        this.listaVeiculos.remove(v.getMatricula(),v);
    }
    
    public void addPedido(Cliente c, Veiculos v){
        this.listapedidos.put(v.getMatricula(),c.clone());
    }
    
    public void remPedido(Cliente c,Veiculos v){
        this.listapedidos.remove(v.getMatricula(),c);
    }
    
    public void addAluguer(Aluguer a){
        this.historico.add(a.clone());
    }
    
    public void classifica(int c){
        this.classificacao = (int)((this.classificacao + c)/2); 
    }
}

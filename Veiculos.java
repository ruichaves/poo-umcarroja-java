import java.io.*;
import java.util.*;

public class Veiculos{
    private String descricao;
    private String matricula;
    private boolean estado;
    private double preco;
    private int velocidade;
    private int consumo;
    private int classificacao;
    private int autonomia;
    private double facturado;
    private Coordenadas local;
    private TreeSet<Aluguer> historico;
    
    public Veiculos(String desc,boolean estado,String matricula,int velocidade,double preco,int consumo,int classificacao,int autonomia,Coordenadas local){
        this.descricao = desc;
        this.matricula = matricula;
        this.estado = estado;
        this.preco = preco;
        this.velocidade = velocidade;
        this.consumo = consumo;
        this.classificacao = classificacao;
        this.autonomia = autonomia;
        this.facturado = 0.0;
        this.local = local;
        this.historico = new TreeSet<Aluguer>(new AluguerComparator());
    }
    public Veiculos(Veiculos v){
        this.descricao = v.getDescricao();
        this.estado = v.getEstado();
        this.matricula = v.getMatricula();
        this.velocidade = v.getVelocidade();
        this.preco = v.getPreco();
        this.consumo = v.getConsumo();
        this.classificacao = v.getClas();
        this.autonomia = v.getAutonomia();
        this.facturado = v.getFacturado();
        this.local = v.getLocal();
        this.historico = v.getHistorico();
    }
    
    public String getDescricao(){return this.descricao;}
    public boolean getEstado(){return this.estado;}
    public String getMatricula(){return this.matricula;}
    public int getVelocidade(){return this.velocidade;}
    public double getPreco(){return this.preco;}
    public int getConsumo(){return this.consumo;}
    public int getAutonomia(){return this.autonomia;}
    public int getClas(){return this.classificacao;}
    public double getFacturado(){return this.facturado;}
    public Coordenadas getLocal(){return this.local;}
    public TreeSet<Aluguer> getHistorico(){
        TreeSet<Aluguer> hist = new TreeSet<Aluguer>(new AluguerComparator());
        Iterator<Aluguer> it = this.historico.iterator();
        while(it.hasNext()){hist.add(it.next());}
        return hist;
    }
    
    public void setDescricao(String desc){this.descricao = desc;}
    public void setEstado(boolean estado){this.estado = estado;}
    public void setMatricula(String mat){this.matricula = mat;}
    public void setVelocidade(int vel){this.velocidade = vel;}
    public void setPreco(double pre){this.preco = pre;}
    public void setConsumo(int cons){this.consumo = cons;}
    public void setAuto(int aut){this.autonomia = aut;}
    public void setClas(int clas){this.classificacao = clas;}
    public void setFacturado(double f){this.facturado = f;}
    public void setLocal(Coordenadas loc){this.local = loc;}
    public void setHistorico(TreeSet<Aluguer> t){
        TreeSet<Aluguer> hist = new TreeSet<Aluguer>(new AluguerComparator());
        Iterator<Aluguer> it = t.iterator();
        while(it.hasNext()){hist.add(it.next());}
        this.historico = hist;
    }
    
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass()))return false;
        Veiculos a = (Veiculos) o;
        return (this.matricula.equals(a.getMatricula()));
    }
    public String toString(){  
        StringBuilder s = new StringBuilder();
        s.append(descricao);
        s.append(System.lineSeparator());
        s.append("Estado: ");
        if (estado) s.append("Livre");
        else s.append("Ocupado");
        s.append(System.lineSeparator());
        s.append("Matricula: ");
        s.append(matricula);
        s.append(System.lineSeparator());
        s.append("Velocidade media por km: ");
        s.append(velocidade);
        s.append(System.lineSeparator());
        s.append("Preco base por km: ");
        s.append(preco);
        s.append(System.lineSeparator());
        s.append("Consumo por km percorrido: ");
        s.append(consumo);
        s.append(System.lineSeparator());
        s.append("Total facturado do carro até ao momento: ");
        s.append(facturado);
        s.append(System.lineSeparator());
        s.append("Classificacao do carro: ");
        s.append(classificacao);
        s.append(System.lineSeparator());
        return s.toString();
    }
    public Veiculos clone(){return new Veiculos(this);}
    
    public void addAluguer(Aluguer a){
        this.historico.add(a.clone());
    }
    
    public void addFacturacao(double p){
        this.facturado += p;
    }
    
    public void classifica(int c){
        this.classificacao = (int)((this.classificacao + c)/2); 
    }
}   

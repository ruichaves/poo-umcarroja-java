import java.io.*;
import java.util.*;
import java.text.*;

public class SISTEMA{
    private static RedeClientes redeC = new RedeClientes();
    private static RedeProprietarios redeP = new RedeProprietarios();
    private static Menu menuLogin, menuCliente, menuAC, menuAluguer, menuProprietario, menuAP,menuV,opVeic, menuSinal, menuAbast, menuPreco, menuPedido;
    
    public static void main(String[] args){
        menus();
        menuInicial();
    }
    
    public static void menus(){
        String [] login = {"Login de Cliente", "Login de Proprietário", "Criar conta como cliente", "Criar conta como Proprietário"};
        String [] cliente = {"Alterar dados", "CARRO JA!", "Classificar viagem", "Ver viagens"};
        String [] dadosC = {"Alterar email", "Alterar Password", "Alterar nome", "Alterar morada", "Alterar data de nascimento", "Alterar localizacao atual"};
        String [] aluguer = {"Mais proximo", "Mais barato", "Mais barato dentro de uma distancia", "Especifico", "Autonomia desejada"};
        String [] proprietario = {"Alterar dados", "Registar Carros", "Ver carros", "Sinalizar veiculos", "Abastacer veiculos", "Alterar preço", "Ver total facturado", "Lista de Pedidos", "Ver viagens", "Ver 10 clientes que mais utilizam"};
        String [] dadosP = {"Alterar email", "Alterar Password", "Alterar nome", "Alterar morada", "Alterar data de nascimento"};
        String [] tipos = {"Registar carro a gasolina", "Registar carro electrico", "Registar carro hibrido"};
        String [] opcao = {"Vender veiculo", "Ver proximo veiculo"};
        String [] sinais = {"Sinalizar como indisponivel", "Sinalizar como disponivel"};
        String [] greve = {"Abastacer gasolina", "Carregar veiculo electrico", "Abastacer carro hibrido"};
        String [] dinheiro = {"Alterar preço base por km"};
        String [] pedido = {"Aceitar viagem", "Rejeitar viagem"};
        menuLogin = new Menu(login);
        menuCliente = new Menu(cliente);
        menuAC = new Menu(dadosC);
        menuAluguer = new Menu (aluguer);
        menuProprietario = new Menu(proprietario);
        menuAP = new Menu(dadosP);
        menuV = new Menu(tipos);
        opVeic = new Menu(opcao);
        menuSinal = new Menu(sinais);
        menuAbast = new Menu(greve);
        menuPreco = new Menu(dinheiro);
        menuPedido = new Menu(pedido);
    }
    
    private static void mensagem(){
        String s;
        Scanner reader = new Scanner(System.in);
        System.out.println("##### Prima 'G' para sair #####");
        do {
            s = reader.nextLine();
        }
        while(s.endsWith("G") != true);
    }

    
    public static void menuInicial(){
        do {
            System.out.print('\u000C');
            System.out.println("############# !!! UM Carro Já !!! ############# \n");
            menuLogin.executa();
            switch (menuLogin.getOp()){
                case 1: loginC();
                case 2: loginP();
                case 3: criaC();
                case 4: criaP();
            }
        }while (menuLogin.getOp()!=0);
        System.out.println("############# Volte sempre ############# \n");
        System.exit(0);
    }
    
    public static void loginC(){
        System.out.print('\u000C');
        int aux=0;
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o email: ");
        String email=ler.nextLine();
        System.out.println("Insira a password: ");
        String pass=ler.nextLine();
        aux = redeC.login(email,pass);
        if(aux==0){
            System.out.println("Password Inválida");
            mensagem();
            menuInicial();
        }
        menuC(email);
    }  
    public static void loginP(){
        System.out.print('\u000C');
        int aux=0;
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o email: ");
        String email=ler.nextLine();
        System.out.println("Insira a password: ");
        String pass=ler.nextLine();
        aux = redeP.login(email,pass);
        if(aux==0){
            System.out.println("Password Inválida");
            mensagem();
            menuInicial();
        }
        menuP(email);
    }
    public static void criaC(){
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o seu email: \n");
        String email = ler.nextLine();
        System.out.println("Insira a sua password: \n");
        String pass = ler.nextLine();
        System.out.println("Insira o seu nome: \n");
        String nome = ler.nextLine();
        System.out.println("Insira a sua morada: \n");
        String morada = ler.nextLine();
        int dia;
        int mes;
        int ano;
        System.out.println("Introduza a sua data de nascimento no formato dia,mes,ano separado por 'Enters': \n");
        while(true){
            dia = ler.nextInt();
            mes = ler.nextInt();
            ano = ler.nextInt();
            if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
        }
        GregorianCalendar dn = new GregorianCalendar(ano,(mes-1),dia);
        int x;
        int y;
        System.out.println("Introduza a sua localização atual no espaco 2D em x e em y separado por 'Enters': \n");
        x = ler.nextInt();
        y = ler.nextInt();
        ler.close();
        Coordenadas l = new Coordenadas(x,y);
        Cliente c = new Cliente (email,nome,pass,morada,dn,l);
        redeC.addCliente(c);
        System.out.println("Cliente criado com sucesso. \n");
        mensagem();
        menuInicial();
    }
    public static void criaP(){
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o seu email: \n");
        String email = ler.nextLine();
        System.out.println("Insira a sua password: \n");
        String pass = ler.nextLine();
        System.out.println("Insira o seu nome: \n");
        String nome = ler.nextLine();
        System.out.println("Insira a sua morada: \n");
        String morada = ler.nextLine();
        int dia;
        int mes;
        int ano;
        System.out.println("Introduza a sua data de nascimento no formato dia,mes,ano separado por 'Enters': \n");
        while(true){
            dia = ler.nextInt();
            mes = ler.nextInt();
            ano = ler.nextInt();
            if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
        }
        GregorianCalendar dn = new GregorianCalendar(ano,(mes-1),dia);
        Proprietario p = new Proprietario (email,nome,pass,morada,dn,0);
        redeP.addProprietario(p);
        ler.close();
        System.out.println("Proprietario criado com sucesso. \n");
        mensagem();
        menuInicial();
    }

    
    public static void menuC(String c){
        do {
            System.out.print('\u000C');
            menuCliente.executa();
            switch (menuCliente.getOp()){
                case 1: alteraC(c);
                break;
                case 2: alugar(c);
                break;
                case 3: classificar(c);
                break;
                case 4: viagensC(c);
                break;
            }
        }while (menuCliente.getOp()!=0);
        menuInicial();
    }
    
    public static void alteraC(String c){
        do {
            System.out.print('\u000C');
            menuAC.executa();
            switch (menuAC.getOp()){
                case 1 : editEmailC(c);
                break;
                case 2 : editPassC(c);
                break;
                case 3 : editNomeC(c);
                break;
                case 4 : editMoradaC(c);
                break;
                case 5 : editNascimentoC(c);
                break;
                case 6 : editLocalC(c);
            }
        } while(menuAC.getOp()!=0);
        menuC(c);
    }
    public static void editEmailC(String c){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente aux = new Cliente(redeC.getCliente(c));
        System.out.println("Insira o novo email");
        String email = ler.nextLine();
        ler.close();
        aux.setEmail(email);
        Cliente del = new Cliente(redeC.getCliente(email));
        if (del != null){
            System.out.println("Email já atribuido. \n");
            mensagem();
            alteraC(c);
        }
        redeC.trocaCliente(aux);
        System.out.println("Email alterado com sucesso. \n");
        mensagem();
        alteraC(email);
    }
    public static void editPassC(String c){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente aux = new Cliente(redeC.getCliente(c));
        System.out.println("Insira a sua nova password");
        String pass = ler.nextLine();
        ler.close();
        aux.setPass(pass);
        redeC.trocaCliente(aux);
        System.out.println("Password alterada com sucesso. \n");
        mensagem();
        alteraC(c);
    }
    public static void editNomeC(String c){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente aux = new Cliente(redeC.getCliente(c));
        System.out.println("Insira o seu novo nome");
        String nome = ler.nextLine();
        ler.close();
        aux.setNome(nome);
        redeC.trocaCliente(aux);
        System.out.println("Nome alterado com sucesso. \n");
        mensagem();
        alteraC(c);
    }
    public static void editMoradaC(String c){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente aux = new Cliente(redeC.getCliente(c));
        System.out.println("Insira a sua nova morada");
        String morada = ler.nextLine();
        ler.close();
        aux.setMorada(morada);
        redeC.trocaCliente(aux);
        System.out.println("Morada alterada com sucesso. \n");
        mensagem();
        alteraC(c);
    }
    public static void editNascimentoC(String c){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente aux = new Cliente(redeC.getCliente(c));
        int dia;
        int mes;
        int ano;
        System.out.println("Introduza a sua nova data de nascimento no formato dia,mes,ano separado por 'Enters': \n");
        while(true){
            dia = ler.nextInt();
            mes = ler.nextInt();
            ano = ler.nextInt();
            if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
        }
        GregorianCalendar dn = new GregorianCalendar(ano,(mes-1),dia);
        ler.close();
        aux.setData(dn);
        redeC.trocaCliente(aux);
        System.out.println("Data de nascimento alterada com sucesso. \n");
        mensagem();
        alteraC(c);
    }
    public static void editLocalC(String c){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente aux = new Cliente(redeC.getCliente(c));
        int x;
        int y;
        System.out.println("Introduza a sua localização atual no espaco 2D em x e em y separado por 'Enters': \n");
        x = ler.nextInt();
        y = ler.nextInt();
        ler.close();
        Coordenadas l = new Coordenadas(x,y);
        aux.setLocal(l);
        
        System.out.println("Mudou de localizacao com sucesso. \n");
        mensagem();
        alteraC(c);
    }
    
    public static void alugar(String c){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o seu destino no espaco 2D em x e em y separado por 'Enters': \n");
        int x = ler.nextInt();
        int y = ler.nextInt();
        ler.close();
        Coordenadas l = new Coordenadas(x,y);
        do{
            System.out.print('\u000C');
            menuAluguer.executa();
            switch (menuAluguer.getOp()) {
                case 1: maisproximo(c,l);
                break;
                case 2: maisbarato(c,l);
                break;
                case 3: maisbarato2(c,l);
                break;
                case 4: especifico(c,l);
                break;
                case 5: autonomia(c,l);
            }
        }while (menuAluguer.getOp()!=0);
        menuC(c);
    }
    public static void maisproximo(String c, Coordenadas dest){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente atual = new Cliente(redeC.getCliente(c));
        Coordenadas cli = atual.getLocal();
        double dist = 13;
        double auxaux = 0;
        TreeMap<String,Proprietario> tm = new TreeMap<String,Proprietario>(redeP.getRede());
        Proprietario pact = null;
        Veiculos v = null;
        if (tm.size() == 0){
            System.out.println("Nenhum veiculo registado. \n");
            mensagem();
            alugar(c);
        }
        else{
            for (Proprietario p : tm.values()){
                HashMap<String,Veiculos> hm = new HashMap<String,Veiculos>(p.getVeiculos());
                for(Veiculos aux: hm.values()){
                    auxaux = cli.distancia(aux.getLocal());
                    if (auxaux < dist){
                        v = aux.clone();
                        pact = p.clone();
                        dist = auxaux;
                    }
                }
            }
        }
        if (v == null & pact == null){
            System.out.println("Não existem veiculos perto de si num raio de 13km. \n");
            mensagem();
            alugar(c);
        }
        else{
            double percurso = v.getLocal().distancia(dest);
            if (v.getAutonomia() < (percurso * v.getConsumo())){
                System.out.println("Veiculo mais proximo não tem autonomia para o seu percurso. \n");
                mensagem();
                alugar(c);
            }
            if (v.getEstado() == false){
                System.out.println("O veiculo esta a ser utilizado quer adicionar a lista de espera? (Sim/Não)\n");
                String resp = ler.nextLine();
                if (resp.equals("Não")){
                    mensagem();
                    alugar(c);
                }
            }
            System.out.println("Veiculo mais proximo" + v.getMatricula() + ".\n");
            System.out.println("Preco da viagem base é" + (percurso*v.getPreco()) + "este valor pode ser superior mediante o transito ou a metereologia. \n");
            pact.addPedido(atual,v);
            redeP.trocaProprietario(pact);
            atual.setDestino(dest);
            redeC.trocaCliente(atual);
            System.out.println("Pedido efectuado com sucesso!");
            mensagem();
            alugar(c);
        }
    }
    public static void maisbarato(String c, Coordenadas dest){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente atual = new Cliente(redeC.getCliente(c));
        Coordenadas cli = atual.getLocal();
        double preco = 1999;
        double precoaux = 0;
        Proprietario pact = null;
        Veiculos v = null;
        TreeMap<String,Proprietario> tm = new TreeMap<String,Proprietario>(redeP.getRede());
        if (tm.size() == 0){
            System.out.println("Nenhum veiculo registado. \n");
            mensagem();
            alugar(c);
        }
        else{
            for (Proprietario p : tm.values()){
                HashMap<String,Veiculos> hm = new HashMap<String,Veiculos>(p.getVeiculos());
                for(Veiculos aux: hm.values()){
                    precoaux = aux.getPreco();
                    if (precoaux < preco){
                        v = aux.clone();
                        pact = p.clone();
                        preco = precoaux;
                    }
                }
            }
        }
        if (v==null && pact==null){
            System.out.println("Não existem veiculos mais baratos do que 1999 euros por km. \n");
            mensagem();
            alugar(c);
        }
        else{            
            double percurso = v.getLocal().distancia(dest);
            if (v.getAutonomia() < (percurso * v.getConsumo())){
                System.out.println("Veiculo mais barato não tem autonomia para o seu percurso. \n");
                mensagem();
                alugar(c);
            }
            if (v.getEstado() == false){
                System.out.println("O veiculo esta a ser utilizado quer adicionar a lista de espera? (Sim/Não)\n");
                String resp = ler.nextLine();
                if (resp.equals("Não")){
                    mensagem();
                    alugar(c);
                }
            }
            System.out.println("Veiculo mais proximo" + v.getMatricula() + ".\n");
            System.out.println("Preco da viagem base é" + (percurso*v.getPreco()) + "este valor pode ser superior mediante o transito ou a metereologia. \n");
            pact.addPedido(atual,v);
            redeP.trocaProprietario(pact);
            atual.setDestino(dest);
            redeC.trocaCliente(atual);
            System.out.println("Pedido efectuado com sucesso!\n");
            mensagem();
            alugar(c);
        }
    }
    public static void maisbarato2(String c, Coordenadas dest){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        double auxaux = 0;
        double precoaux = 0;
        double preco = 1999;
        Veiculos v = null;
        Proprietario pact = null;
        System.out.println("Qual é a distancia maxima que esta disposto a percorrer? \n");
        double maxdis = ler.nextDouble();
        ler.close();
        if (maxdis <= 0){
            System.out.println("Seja realista. \n");
            mensagem();
            alugar(c);
        }
        Cliente atual = new Cliente(redeC.getCliente(c));
        Coordenadas cli = atual.getLocal();
        TreeMap<String,Proprietario> tm = new TreeMap<String,Proprietario>(redeP.getRede());
        if (tm.size() == 0){
            System.out.println("Nenhum veiculo registado. \n");
            mensagem();
            alugar(c);
        }
        else{
           for (Proprietario p : tm.values()){
               HashMap<String,Veiculos> hm = new HashMap<String,Veiculos>(p.getVeiculos());
               for(Veiculos aux: hm.values()){
                    auxaux = cli.distancia(aux.getLocal());
                    precoaux = aux.getPreco();
                    if (auxaux < maxdis && precoaux < preco){
                        v = aux.clone();
                        pact = p.clone();
                        preco = precoaux;
                    }
               }
           }
        }
        if (v==null && pact==null){
            System.out.println("Não existem veiculos disponiveis mais baratos do que 1999 euros por km num raio de " + maxdis + ". \n");
            mensagem();
            alugar(c);
        }
        else{
            double percurso = v.getLocal().distancia(dest);
            if (v.getAutonomia() < (percurso * v.getConsumo())){
                System.out.println("Veiculo mais barato a distancia que esta disposto a percorrer não tem autonomia para o seu percurso. \n");
                mensagem();
                alugar(c);
            }
            if (v.getEstado() == false){
                System.out.println("O veiculo esta a ser utilizado quer adicionar a lista de espera? (Sim/Não)\n");
                String resp = ler.nextLine();
                if (resp.equals("Não")){
                    mensagem();
                    alugar(c);
                }
            }
            System.out.println("Veiculo mais barato" + v.getMatricula() + ".\n");
            System.out.println("Preco da viagem base é" + (percurso*v.getPreco()) + "este valor pode ser superior mediante o transito ou a metereologia. \n");
            pact.addPedido(atual,v);
            redeP.trocaProprietario(pact);
            atual.setDestino(dest);
            redeC.trocaCliente(atual);
            System.out.println("Pedido efectuado com sucesso!");
            mensagem();
            alugar(c);
        } 
    }
    public static void especifico(String c, Coordenadas dest){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente atual = new Cliente(redeC.getCliente(c));
        Coordenadas cli = atual.getLocal();
        TreeMap<String,Proprietario> tm = new TreeMap<String,Proprietario>(redeP.getRede());
        Veiculos v = null;
        Proprietario pact = null;
        if (tm.size() == 0){
            System.out.println("Nenhum veiculo registado. \n");
            mensagem();
            alugar(c);
        }
        else{
            System.out.println("Qual é a matricula do carro que pretende alugar? \n");
            String mat = ler.nextLine();
            ler.close();
            for (Proprietario p : tm.values()){
               HashMap<String,Veiculos> hm = new HashMap<String,Veiculos>(p.getVeiculos());
               for (Veiculos aux: hm.values()){
                   if (aux.getMatricula().equals(mat)){
                       v = aux.clone();
                       pact = p.clone();
                       break;
                   }
               }
           }
        }
        if (v == null && pact == null){
            System.out.println("Não existe nenhum veiculo disponivel com essa matricula. \n");
            mensagem();
            alugar(c);
        }
        else{
            double percurso = v.getLocal().distancia(dest);
            if (v.getAutonomia() < (percurso * v.getConsumo())){
                System.out.println("Esse veiculo especifico não tem autonomia para o seu percurso. \n");
                mensagem();
                alugar(c);
            }
            if (v.getEstado() == false){
                System.out.println("O veiculo esta a ser utilizado quer adicionar a lista de espera? (Sim/Não)\n");
                String resp = ler.nextLine();
                if (resp.equals("Não")){
                    mensagem();
                    alugar(c);
                }
            }
            System.out.println("Veiculo requisitado" + v.getMatricula() + ".\n");
            System.out.println("Preco da viagem base é" + (percurso*v.getPreco()) + "este valor pode ser superior mediante o transito ou a metereologia. \n");
            pact.addPedido(atual,v);
            redeP.trocaProprietario(pact);
            atual.setDestino(dest);
            redeC.trocaCliente(atual);
            System.out.println("Pedido efectuado com sucesso!");
            mensagem();
            alugar(c);
        }
    }
    public static void autonomia(String c, Coordenadas dest){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Cliente atual = new Cliente(redeC.getCliente(c));
        Coordenadas cli = atual.getLocal();
        TreeMap<String,Proprietario> tm = new TreeMap<String,Proprietario>(redeP.getRede());
        Veiculos v = null;
        Proprietario pact = null;
        if(tm.size() == 0){
            System.out.println("Nenhum veiculo registado. \n");
            mensagem();
            alugar(c);
        }
        else{
           System.out.println("Qual é a autonomia que pretende ter no veiculo? \n");
           int aut = ler.nextInt();
           ler.close();
           if (aut <= 0 && aut >=1000){
               System.out.println("Seja realista. \n");
               mensagem();
               alugar(c);
           }
           for (Proprietario p : tm.values()){
               HashMap<String,Veiculos> hm = new HashMap<String,Veiculos>(p.getVeiculos());
               for (Veiculos aux: hm.values()){
                   if (aux.getAutonomia() == aut){
                       v = aux.clone();
                       pact = p.clone();
                   }
               }
           }
        }
        if (v == null && pact == null){
            System.out.println("Não existe nenhum veiculo disponivel com essa autonomia. \n");
            mensagem();
            alugar(c);
        }
        else{
            double percurso = v.getLocal().distancia(dest);
            if (v.getAutonomia() < (percurso * v.getConsumo())){
                System.out.println("Autonomia indicada não é suficiente para o seu percurso. \n");
                mensagem();
                alugar(c);
            }
            System.out.println("Veiculo requisitado" + v.getMatricula() + ".\n");
            System.out.println("Preco da viagem base é" + (percurso*v.getPreco()) + "este valor pode ser superior mediante o transito ou a metereologia. \n");
            pact.addPedido(atual,v);
            redeP.trocaProprietario(pact);
            atual.setDestino(dest);
            redeC.trocaCliente(atual);
            System.out.println("Pedido efectuado com sucesso!");
            mensagem();
            alugar(c);
        }
    }
    
    public static void classificar(String c){
        Scanner ler = new Scanner(System.in);
        Cliente user = redeC.getCliente(c);
        TreeSet<Aluguer> histUser = user.getHistorico();
        Iterator<Aluguer> it = histUser.descendingIterator();
        TreeMap<String,Proprietario> tm = new TreeMap<String,Proprietario>(redeP.getRede());
        int dia=0,mes=0,ano=0;
        boolean flag = true;
        int cp,cv;
        Proprietario dono = null;
        Veiculos v = null;
        Aluguer procurado = null;
        if (histUser.isEmpty()){
            System.out.println("Nao tem nenhuma viagem realizada.\n");
            mensagem();
            menuC(c);
        }
        else{
            System.out.println("A matricula do veiculo que pretende classificar:");
            String mat = ler.nextLine();
            for (Proprietario p : tm.values()){
               HashMap<String,Veiculos> hm = new HashMap<String,Veiculos>(p.getVeiculos());
               if (hm.containsKey(mat)){
                   v = hm.get(mat).clone();
                   dono = p.clone();
               } 
            }
            if (v == null){
                System.out.println("A matricula do veiculo que indicou nao existe.");
                mensagem();
                menuC(c);
            }
            System.out.println("Introduza a data da viagem no formato dia,mes,ano separado por 'Enters': \n");
            while(true){
                dia = ler.nextInt();
                mes = ler.nextInt();
                ano = ler.nextInt();
                if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
            }
            GregorianCalendar d = new GregorianCalendar(ano,(mes-1),dia);
            System.out.println("Introduza as coordenadas iniciais da viagem pretendida separado por 'Enters'.\n");
            int x1 = ler.nextInt();
            int y1 = ler.nextInt();
            Coordenadas i = new Coordenadas(x1,y1);
            System.out.println("Introduza as coordenadas finais da viagem pretendida separado por 'Enters'.\n");
            int x2 = ler.nextInt();
            int y2 = ler.nextInt();
            Coordenadas f = new Coordenadas(x2,y2);
            Aluguer aux = new Aluguer(c,v,d,i,f,0.0,0.0,0);
            while(it.hasNext() && flag){
                Aluguer a = it.next();
                if (a.equals(aux) && a.getFlag() == 0){
                    flag = false;
                    procurado = new Aluguer(a);
                }
            }
            if (flag){
                System.out.println("Não existe nenhum aluguer com essas caracteristicas.");
                mensagem();
                menuC(c);
            }
            while(true){
                System.out.println("De 0 a 10 quanto avalia o veiculo " + mat + ": \n");
                cv = ler.nextInt();
                if (cv>=0 && cv <= 10) break;
            }
            while(true){
                System.out.println("De 0 a 10 quanto avalia o proprietario " + dono.getEmail() + ": \n");
                cp = ler.nextInt();
                if (cp>=0 && cp <= 10) break;
            }
            ler.close();
            histUser.remove(procurado);
            procurado.setFlag(1);
            user.addAluguer(procurado);
            redeC.trocaCliente(user);
            dono.classifica(cp);
            v.classifica(cv);
            HashMap<String,Veiculos> listaVeiculos = new HashMap<String,Veiculos>(dono.getVeiculos());
            listaVeiculos.put(mat,v);
            dono.setVeiculos(listaVeiculos);
            redeP.trocaProprietario(dono);
            System.out.println("Classificacao foi atribuida com sucesso!");
            menuC(c);
        }
    }
    
    public static void viagensC(String c){
        Scanner ler = new Scanner(System.in);
        TreeSet<Aluguer> aux = redeC.getCliente(c).getHistorico();
        TreeSet<Aluguer> auxaux = new TreeSet<Aluguer>(new AluguerComparator());
        Iterator<Aluguer> it = aux.descendingIterator();
        Iterator<Aluguer> it1 = auxaux.descendingIterator();
        int dia=0,mes=0,ano=0;
        int dia1=0,mes1=0,ano1=0;
        if(aux.isEmpty()){
            System.out.println("Nao tem nenhum aluguer registado");
            mensagem();
            menuC(c);
        }else{
            System.out.println("Introduza a data inicial no formato dia,mes,ano separado por 'Enters': \n");
            while(true){
                dia = ler.nextInt();
                mes = ler.nextInt();
                ano = ler.nextInt();
                if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
            }
            GregorianCalendar di = new GregorianCalendar(ano,(mes-1),dia);
            System.out.println("Introduza a data final no formato dia,mes,ano separado por 'Enters': \n");
            while(true){
                dia1 = ler.nextInt();
                mes1 = ler.nextInt();
                ano1 = ler.nextInt();
                if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
            }
            GregorianCalendar df = new GregorianCalendar(ano1,(mes1-1),dia1);
            while(it.hasNext()){
                Aluguer a = it.next();
                if (a.getData().after(di) && a.getData().before(df)){
                    auxaux.add(a.clone());
                }
            }
            if (auxaux.size()==0){
                System.out.println("Nao tem nenhum aluguer registado entre essas datas");
                mensagem();
                menuC(c);
            }
            else{
                while(it1.hasNext()){
                    Aluguer a = it.next();
                    System.out.println(a.toString());
                    System.lineSeparator();
                }
            }
            menuC(c);
        }
    }
    
    
    public static void menuP(String p){
        do {
            System.out.print('\u000C');
            menuProprietario.executa();
            switch (menuProprietario.getOp()){
                case 1: alteraP(p);
                break;
                case 2: registaV(p);
                break;
                case 3: verV(p);
                break;
                case 4: sinalizarV(p);
                break;
                case 5: abastacerV(p);
                break;
                case 6: precoV(p);
                break;
                case 7: verFacturado(p);
                break;
                case 8: pedidosV(p);
                break;
                case 9: viagensP(p);
                break;
                case 10: top10(p);
                break;
            }
        }while (menuProprietario.getOp()!=0);
        menuInicial();
    }
    
    public static void alteraP(String p){
        do {
            System.out.print('\u000C');
            menuAP.executa();
            switch (menuAP.getOp()){
                case 1 : editEmailP(p);
                break;
                case 2 : editPassP(p);
                break;
                case 3 : editNomeP(p);
                break;
                case 4 : editMoradaP(p);
                break;
                case 5 : editNascimentoP(p);
                break;
            }
        } while(menuAP.getOp()!=0);
        menuP(p);
    }
    public static void editEmailP(String p){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        System.out.println("Insira o novo email");
        String email = ler.nextLine();
        ler.close();
        aux.setEmail(email);
        Proprietario del = new Proprietario(redeP.getProprietario(email));
        if (del != null){
            System.out.println("Email já atribuido. \n");
            mensagem();
            alteraP(p);
        }
        redeP.trocaProprietario(aux);
        System.out.println("Email alterado com sucesso. \n");
        mensagem();
        alteraP(email);
    }
    public static void editPassP(String p){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        System.out.println("Insira a sua nova password");
        String pass = ler.nextLine();
        ler.close();
        aux.setPass(pass);
        redeP.trocaProprietario(aux);
        System.out.println("Password alterado com sucesso. \n");
        mensagem();
        alteraP(p);
    }
    public static void editNomeP(String p){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        System.out.println("Insira o seu novo nome");
        String nome = ler.nextLine();
        ler.close();
        aux.setNome(nome);
        redeP.trocaProprietario(aux);
        System.out.println("Nome alterado com sucesso. \n");
        mensagem();
        alteraP(p);
    }
    public static void editMoradaP(String p){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        System.out.println("Insira a sua nova morada");
        String morada = ler.nextLine();
        ler.close();
        aux.setMorada(morada);
        redeP.trocaProprietario(aux);
        System.out.println("Morada alterada com sucesso. \n");
        mensagem();
        alteraP(p);
    }
    public static void editNascimentoP(String p){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        int dia;
        int mes;
        int ano;
        System.out.println("Introduza a sua nova data de nascimento no formato dia,mes,ano separado por 'Enters': \n");
        while(true){
            dia = ler.nextInt();
            mes = ler.nextInt();
            ano = ler.nextInt();
            if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
        }
        GregorianCalendar dn = new GregorianCalendar(ano,(mes-1),dia);
        ler.close();
        aux.setData(dn);
        redeP.trocaProprietario(aux);
        System.out.println("Data de nascimento alterada com sucesso. \n");
        mensagem();
        alteraP(p);
    }
    
    public static void registaV(String p){
        do {
            System.out.print('\u000C');
            menuV.executa();
            switch (menuV.getOp()){
                case 1 : criaGasolina(p);
                break;
                case 2 : criaElectrico(p);
                break;
                case 3 : criaHibrido(p);
                break;
            }
        } while(menuV.getOp()!=0);
        menuP(p);
    }
    public static void criaGasolina(String p){
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        System.out.println("Descricao do carro: \n");
        String desc = ler.nextLine();
        System.out.println("Insira a matricula: \n");
        String mat = ler.nextLine();
        System.out.println("Insira a velocidade media por km/h: \n");
        int vel = ler.nextInt();
        System.out.println("Insira o preço base por km");
        double pre = ler.nextDouble();
        System.out.println("Insira consumo de gasolina por km percorrido");
        int cons = ler.nextInt();
        System.out.println("Insira autonomia: ");
        int aut = ler.nextInt();
        System.out.println("Introduza a sua localização atual no espaco 2D em x e em y separado por 'Enters': \n");
        int x = ler.nextInt();
        int y = ler.nextInt();
        ler.close();
        Coordenadas lv = new Coordenadas(x,y);
        CarroGasolina cg = new CarroGasolina(desc,true,mat,vel,pre,cons,0,aut,lv);
        aux.addVeiculo(cg);
        redeP.trocaProprietario(aux);
        mensagem();
        menuP(p);
    }  
    public static void criaElectrico(String p){
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        System.out.println("Descricao do carro: \n");
        String desc = ler.nextLine();
        System.out.println("Insira a matricula: \n");
        String mat = ler.nextLine();
        System.out.println("Insira a velocidade media por km/h: \n");
        int vel = ler.nextInt();
        System.out.println("Insira o preço base por km");
        double pre = ler.nextDouble();
        System.out.println("Insira consumo de gasolina por km percorrido");
        int cons = ler.nextInt();
        System.out.println("Insira autonomia: ");
        int aut = ler.nextInt();
        System.out.println("Introduza a sua localização atual no espaco 2D em x e em y separado por 'Enters': \n");
        int x = ler.nextInt();
        int y = ler.nextInt();
        ler.close();
        Coordenadas lv = new Coordenadas(x,y);
        CarroElectrico ce = new CarroElectrico(desc,true,mat,vel,pre,cons,0,aut,lv);
        aux.addVeiculo(ce);
        redeP.trocaProprietario(aux);
        mensagem();
        menuP(p);
    }
    public static void criaHibrido(String p){
        Scanner ler = new Scanner(System.in);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        System.out.println("Descricao do carro: \n");
        String desc = ler.nextLine();
        System.out.println("Insira a matricula: \n");
        String mat = ler.nextLine();
        System.out.println("Insira a velocidade media por km/h: \n");
        int vel = ler.nextInt();
        System.out.println("Insira o preço base por km");
        double pre = ler.nextDouble();
        System.out.println("Insira consumo de gasolina por km percorrido");
        int cons = ler.nextInt();
        System.out.println("Insira autonomia: ");
        int aut = ler.nextInt();
        System.out.println("Introduza a sua localização atual no espaco 2D em x e em y separado por 'Enters': \n");
        int x = ler.nextInt();
        int y = ler.nextInt();
        ler.close();
        Coordenadas lv = new Coordenadas(x,y);
        CarroHibrido ch = new CarroHibrido(desc,true,mat,vel,pre,cons,0,aut,lv);
        aux.addVeiculo(ch);
        redeP.trocaProprietario(aux);
        mensagem();
        menuP(p);
    }
    
    public static void verV(String p){
        System.out.print('\u000C');
        int flag=1;
        HashMap<String,Veiculos> aux = redeP.getProprietario(p).getVeiculos();
        if(aux.size()==0){
            System.out.println("Nao tem veiculos associados. \n");
            mensagem();
            menuP(p);
        }
        else{
            for(Veiculos v: aux.values()){
                if (flag==0) menuP(p);
                else flag = menuVeiculos(p,v);
            }
            if (flag==0) menuP(p);
            else{
                System.out.println("Não existe mais veiculos associados. \n");
                mensagem();
                menuP(p);
            }
        }
    }
    public static int menuVeiculos(String p, Veiculos v){
        int op;
        do {
            System.out.println(v.toString());
            opVeic.executa(); 
            switch (op=opVeic.getOp()) {
                case 1: apagarVeiculo(p,v);
                break;
                case 2: System.out.print('\u000C');;
                break;
            }
        }while (opVeic.getOp()!=1 && opVeic.getOp()!=2 && opVeic.getOp()!=0);
        return op;
    }
    public static void apagarVeiculo(String p,Veiculos v){
        System.out.print('\u000C');
        Proprietario aux = new Proprietario (redeP.getProprietario(p));
        aux.remVeiculo(v);
        redeP.trocaProprietario(aux);
        System.out.println("Veiculo removido com sucesso");
        mensagem();
        menuP(p);
    }
    
    public static void sinalizarV(String p){
        Scanner ler = new Scanner(System.in);
        System.out.print('\u000C');
        int flag=1;
        HashMap<String,Veiculos> aux = new HashMap <String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        if(aux.size()==0){
            System.out.println("Nao tem veiculos associados. \n");
            mensagem();
            menuP(p);
        }
        else{
            System.out.println("Qual o veiculo que pretende marcar, insira a matricula? \n");
            String mat = ler.nextLine();
            if (aux.containsKey(mat)){
                Veiculos v = aux.get(mat);
                menuIsp(p,v);
                mensagem();
                menuP(p);
            }
            else{
                System.out.println("Nao tem nenhum veiculo com essa matricula. \n");
                mensagem();
                sinalizarV(p);
            }
        }
    }
    public static void menuIsp(String p,Veiculos v){
        int op;
        do {
            menuSinal.executa(); 
            switch (op=menuSinal.getOp()) {
                case 1: marcaIndis(p,v);
                break;
                case 2: marcaDisp(p,v);;
                break;
            }
        }while (menuSinal.getOp()!=1 && menuSinal.getOp()!=2 && menuSinal.getOp()!=0);
    }
    public static void marcaIndis(String p,Veiculos v){
        Veiculos aux = new Veiculos(v);
        aux.setEstado(false);
        HashMap<String,Veiculos> hm = new HashMap <String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        hm.replace(aux.getMatricula(),v,aux);
        Proprietario pai = new Proprietario(redeP.getProprietario(p));
        pai.setVeiculos(hm);
        redeP.trocaProprietario(pai);
        System.out.println("Veiculo " + v.getMatricula() + "marcado como indisponivel. \n");
    }
    public static void marcaDisp(String p,Veiculos v){
        Veiculos aux = new Veiculos(v);
        aux.setEstado(true);
        HashMap<String,Veiculos> hm = new HashMap <String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        hm.replace(aux.getMatricula(),v,aux);
        Proprietario pai = new Proprietario(redeP.getProprietario(p));
        pai.setVeiculos(hm);
        redeP.trocaProprietario(pai);
        System.out.println("Veiculo " + v.getMatricula() + "marcado como disponivel. \n");
    }
    
    public static void abastacerV(String p){
        Scanner ler = new Scanner(System.in);
        System.out.print('\u000C');
        int flag=1;
        HashMap<String,Veiculos> aux = new HashMap <String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        if(aux.size()==0){
            System.out.println("Nao tem veiculos associados. \n");
            mensagem();
            menuP(p);
        }
        else{
            menuA(p,aux);
            mensagem();
            menuP(p);
        }
    }
    public static void menuA(String p, HashMap<String,Veiculos> hm){
        int op;
        HashMap <String,Veiculos> hcg = new HashMap<String,Veiculos>();
        HashMap <String,Veiculos> hce = new HashMap<String,Veiculos>();
        HashMap <String,Veiculos> hch = new HashMap<String,Veiculos>();
        for(Veiculos v : hm.values()){
            if (v.getClass().getName().equals("CarroGasolina")) hcg.put(v.getMatricula(),v);
            if (v.getClass().getName().equals("CarroElectrico")) hce.put(v.getMatricula(),v);
            if (v.getClass().getName().equals("CarroHibrido")) hch.put(v.getMatricula(),v);
        }
        do {
            menuAbast.executa(); 
            switch (op=menuAbast.getOp()) {
                case 1: abasteceG(p,hcg,hm);
                case 2: abasteceE(p,hce,hm);
                case 3: abasteceH(p,hch,hm);
                break;
            }
        }while (menuAbast.getOp()!=1 && menuAbast.getOp()!=2 && menuAbast.getOp()!=3 && menuAbast.getOp()!=0);
    }
    public static void abasteceG(String p, HashMap<String,Veiculos> hcg, HashMap<String,Veiculos> hm){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira a matricula do veiculo que pretende abastacer: \n");
        String mat = ler.nextLine();
        if (hcg.containsKey(mat)){
            System.out.println("Insira quanto pretende abastacer: \n");
            int aut = ler.nextInt();
            if (aut >= 1000 && aut <= 0){
                System.out.println("Seja realista: \n");
                mensagem();
                menuA(p,hm);
            }
            ler.close();
        }
        else{
            System.out.println("Não existe nenhum carro a gasolina com essa matricula: \n");
            mensagem();
            menuA(p,hm);
        }
    }
    public static void abasteceE(String p, HashMap<String,Veiculos> hce, HashMap<String,Veiculos> hm){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira a matricula do veiculo que pretende abastacer: \n");
        String mat = ler.nextLine();
        if (hce.containsKey(mat)){
            System.out.println("Insira quanto pretende recarregar: \n");
            int aut = ler.nextInt();
            ler.close();
            if (aut >= 1000 && aut <= 0){
                System.out.println("Seja realista: \n");
                mensagem();
                menuA(p,hm);
            }
            
        }
        else{
            System.out.println("Não existe nenhum carro electrico com essa matricula: \n");
            mensagem();
            menuA(p,hm);
        }
    }
    public static void abasteceH(String p, HashMap <String,Veiculos> hch, HashMap<String,Veiculos> hm){
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira a matricula do veiculo que pretende abastacer: \n");
        String mat = ler.nextLine();
        if (hch.containsKey(mat)){
            System.out.println("Insira quanto pretende recarregar: \n");
            int aut = ler.nextInt();
            if (aut >= 1000 && aut <= 0){
                System.out.println("Seja realista: \n");
                mensagem();
                abasteceH(p,hch,hm);
            }
            ler.close();
            
        }
        else{
            System.out.println("Não existe nenhum carro hibrido com essa matricula: \n");
            mensagem();
            menuA(p,hm);
        }
    }
    
    public static void precoV(String p){
        int op;
        System.out.print('\u000C');
        do {
            menuPreco.executa(); 
            switch (op=menuPreco.getOp()) {
                case 1: alteraPreco(p);
                break;
            }
        }while (menuPreco.getOp()!=1 && menuPreco.getOp()!=0);
        mensagem();
        menuP(p);
    }
    public static void alteraPreco(String p){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        HashMap<String,Veiculos> aux = new HashMap <String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        System.out.println("Insira a matricula do veiculo que pretende alterar o preço: \n");
        String mat = ler.nextLine();
        if (aux.containsKey(mat)){
            Veiculos v = aux.get(mat);
            double antigo = v.getPreco();
            System.out.println("Preço antigo:" + antigo + ". \n");
            System.out.println("Insira o novo preco: \n");
            double novo = ler.nextDouble();
            Veiculos vaux = new Veiculos(v);
            vaux.setPreco(novo);
            aux.replace(mat,v,vaux);
            Proprietario pai = new Proprietario(redeP.getProprietario(p));
            pai.setVeiculos(aux);
            redeP.trocaProprietario(pai);
        }
        else{
            System.out.println("Não existe nenhum veiculo com essa matricula: \n");
            mensagem();
            precoV(p);
        }
    }
    
    public static void verFacturado(String p){
        System.out.print('\u000C');
        Scanner ler = new Scanner(System.in);
        HashMap<String,Veiculos> hm = new HashMap <String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        System.out.println("Insira a matricula do veiculo que pretende ver o quanto já facturou:");
        String mat = ler.nextLine();
        if (hm.containsKey(mat)){
            Veiculos v = hm.get(mat);
            double f = v.getFacturado();
            System.out.println ("Total facturado pelo veiculo" + mat + " é " + f);
            mensagem();
            menuP(p);
        }
        else{
            System.out.println("Não existe nenhum veiculo com essa matricula: \n");
            mensagem();
            verFacturado(p);
        }
    }
    
    public static void pedidosV(String p){
        System.out.print('\u000C');
        int f = 0;
        double tempo;
        double dist;
        HashMap<String,Cliente> hmc = new HashMap<String,Cliente>(redeP.getProprietario(p).getPedidos());
        HashMap<String,Veiculos> hmv = new HashMap<String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        if(hmc.size()==0){
            System.out.println("Nenhum veiculo requisitado.");
            mensagem();
            menuP(p);
        }
        else{
            for(String aux: hmc.keySet()){
                Cliente auxaux = hmc.get(aux);
                String mail = auxaux.getEmail();
                Coordenadas d = auxaux.getDestino();
                Coordenadas c = auxaux.getLocal();
                Veiculos auxauxaux = hmv.get(aux);
                Coordenadas v = auxauxaux.getLocal();
                dist = c.distancia(v);
                tempo = ((dist/4) * 60);
                System.out.println("O cliente" + mail + "pretende alugar o veiculo" + aux + " para ir até" + d.toString() + ".\n");
                System.out.println("O cliente vai demorar cerca de " + tempo + "min até chegar. \n");
                execGestaoPedidos(p,auxaux,aux);
            }
        }
        menuP(p);
    }
    public static void execGestaoPedidos(String p, Cliente c, String mat){
        do {
            menuPedido.executa(); 
            switch (menuPedido.getOp()) {
                case 1: aceitarPedido(p,c,mat);
                break;
                case 2: rejeitarPedido(p,c,mat);
                break;
            }
        }while (menuPedido.getOp()!=1 && menuPedido.getOp()!=2 && menuPedido.getOp()!=0);
    }
    public static void aceitarPedido(String p, Cliente c, String mat){
        Random rand = new Random();
        int rand_int = rand.nextInt(4);
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        Veiculos auxaux = new Veiculos(redeP.getProprietario(p).getVeiculos().get(mat));
        HashMap<String,Veiculos> hm = new HashMap <String,Veiculos>(redeP.getProprietario(p).getVeiculos());
        hm.remove(mat);
        Cliente auxauxaux = new Cliente (c);
        auxaux.setEstado(false);
        GregorianCalendar data = new GregorianCalendar();
        double dist = auxaux.getLocal().distancia(c.getDestino());
        int temp = (int) (dist / auxaux.getVelocidade()) * 60;
        double preco = dist * auxaux.getPreco();
        if (rand_int == 1) temp = temp + 5;
        if (rand_int == 2) temp = temp + 10; preco = preco + 0.5;
        if (rand_int == 3) temp = temp + 15; preco = preco + 1.0;
        Aluguer novo = new Aluguer(auxauxaux.getEmail(),auxaux,data,auxaux.getLocal(),c.getDestino(),dist,preco,temp);
        int gasto = auxaux.getConsumo() * ((int) dist);
        int newautonomia = auxaux.getAutonomia() -  gasto;
        if (newautonomia > 0.1*auxaux.getAutonomia()) auxaux.setEstado(true);
        else{
            System.out.println("Posteriormente tem de abastacer o veiculo. \n");
        }
        aux.remPedido(auxauxaux,auxaux);
        aux.addAluguer(novo);
        auxaux.setAuto(newautonomia);
        auxaux.addFacturacao(gasto);
        auxaux.setLocal(c.getDestino());
        auxaux.addAluguer(novo);
        auxauxaux.setLocal(c.getDestino());
        auxauxaux.setDestino(null);
        auxauxaux.addAluguer(novo);
        hm.put(mat,auxaux);
        aux.setVeiculos(hm);
        redeP.trocaProprietario(aux);
        redeC.trocaCliente(auxauxaux);
        System.out.println("Viagem concluida! \n");
        mensagem();
    }
    public static void rejeitarPedido(String p, Cliente c, String mat){
        Proprietario aux = new Proprietario(redeP.getProprietario(p));
        Veiculos auxaux = new Veiculos(redeP.getProprietario(p).getVeiculos().get(mat));
        Cliente auxauxaux = new Cliente(c);
        aux.remPedido(auxauxaux,auxaux);
        redeP.trocaProprietario(aux);
        auxauxaux.setDestino(null);
        redeC.trocaCliente(auxauxaux);
    }
    
    public static void viagensP(String p){
        Scanner ler = new Scanner(System.in);
        TreeSet<Aluguer> aux = redeP.getProprietario(p).getHistorico();
        TreeSet<Aluguer> auxaux = new TreeSet<Aluguer>(new AluguerComparator());
        Iterator<Aluguer> it = aux.descendingIterator();
        Iterator<Aluguer> it1 = auxaux.descendingIterator();
        int dia=0,mes=0,ano=0;
        int dia1=0,mes1=0,ano1=0;
        if(aux.isEmpty()){
            System.out.println("Nao tem nenhum aluguer registado");
            mensagem();
            menuP(p);
        }else{
            System.out.println("Introduza a data inicial no formato dia,mes,ano separado por 'Enters': \n");
            while(true){
                dia = ler.nextInt();
                mes = ler.nextInt();
                ano = ler.nextInt();
                if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
            }
            GregorianCalendar di = new GregorianCalendar(ano,(mes-1),dia);
            System.out.println("Introduza a data final no formato dia,mes,ano separado por 'Enters': \n");
            while(true){
                dia1 = ler.nextInt();
                mes1 = ler.nextInt();
                ano1 = ler.nextInt();
                if (dia>=1 && dia <=31 && mes >=1 && mes <=12) break;
            }
            GregorianCalendar df = new GregorianCalendar(ano1,(mes1-1),dia1);
            while(it.hasNext()){
                Aluguer a = it.next();
                if (a.getData().after(di) && a.getData().before(df)){
                    auxaux.add(a.clone());
                }
            }
            if (auxaux.size()==0){
                System.out.println("Nao tem nenhum aluguer registado entre essas datas");
                mensagem();
                menuP(p);
            }
            else{
                while(it1.hasNext()){
                    Aluguer a = it.next();
                    System.out.println(a.toString());
                    System.lineSeparator();
                }
            }
            menuP(p);
        }
    }
    
    public static void top10(String p){
        int i = 0;
        ArrayList<Cliente> top = new ArrayList<Cliente>();
        TreeMap<String,Cliente> tm = new TreeMap<String,Cliente>(redeC.getRede());
        if (tm.size() == 0){
            System.out.println("Nao existem clientes registados.");
            mensagem();
            menuP(p);
        }
        else{
            for (Cliente c: tm.values()){
                if (top.size() <= 10){
                    top.add(c.clone());
                    Collections.sort (top, new Comparator() {
                        public int compare(Object o1, Object o2) {
                            Cliente c1 = (Cliente) o1;
                            Cliente c2 = (Cliente) o2;
                            return c1.getHistorico().size() < c1.getHistorico().size() ? -1 : (c1.getHistorico().size() > c2.getHistorico().size() ? +1 : 0);
                        }
                    });
                }
                else {
                    if (top.get(0).getHistorico().size() <= c.getHistorico().size()){
                        top.remove(0);
                        top.add(0,c.clone());
                        Collections.sort (top, new Comparator() {
                            public int compare(Object o1, Object o2) {
                                Cliente c1 = (Cliente) o1;
                                Cliente c2 = (Cliente) o2;
                                return c1.getHistorico().size() < c1.getHistorico().size() ? -1 : (c1.getHistorico().size() > c2.getHistorico().size() ? +1 : 0);
                            }
                        });
                    }
                }
            }
            while(i < 10){
                System.out.println(top.get(i).toString());
                i++;
            }
        }
    }
 }

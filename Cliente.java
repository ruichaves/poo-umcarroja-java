import java.io.*;
import java.util.*;
import java.text.*;

public class Cliente{
    private String email;
    private String nome;
    private String password;
    private String morada;
    private GregorianCalendar dataNascimento;
    private Coordenadas local;
    private Coordenadas destino;
    private TreeSet<Aluguer> historico;
    
    public Cliente(String e, String n, String p, String m, GregorianCalendar d, Coordenadas l){
        this.email = e;
        this.nome = n;
        this.password = p;
        this.morada = m;
        this.dataNascimento = d;
        this.local = l;
        this.destino = null;
        this.historico = new TreeSet<Aluguer>(new AluguerComparator());
    }
    public Cliente(Cliente c){
        this.email = c.getEmail();
        this.nome = c.getNome();
        this.password = c.getPass();
        this.morada = c.getMorada();
        this.dataNascimento = c.getData();
        this.local = c.getLocal();
        this.destino = c.getDestino();
        this.historico = c.getHistorico();
    }
    
    public String getEmail(){return this.email;}
    public String getNome(){return this.nome;}
    public String getPass(){return this.password;}
    public String getMorada(){return this.morada;}
    public GregorianCalendar getData(){ return (GregorianCalendar) this.dataNascimento.clone();}
    public Coordenadas getLocal(){return this.local.clone();}
    public Coordenadas getDestino(){return this.destino.clone();}
    public TreeSet<Aluguer> getHistorico(){
        TreeSet<Aluguer> hist = new TreeSet<Aluguer>(new AluguerComparator());
        Iterator<Aluguer> it = this.historico.iterator();
        while(it.hasNext()){hist.add(it.next());}
        return hist;
    }
    public void setEmail(String email){this.email = email;}
    public void setNome(String nome){this.nome = nome;}
    public void setPass(String pass){this.password = pass;}
    public void setMorada(String morada){this.morada = morada;}
    public void setData(GregorianCalendar data){this.dataNascimento = data;}
    public void setLocal(Coordenadas local){this.local = local;}
    public void setDestino(Coordenadas dest){this.destino = dest;}
    public void setHistorico(TreeSet<Aluguer> hist){
        TreeSet<Aluguer> h= new TreeSet<Aluguer>(new AluguerComparator());
        Iterator<Aluguer> it = hist.iterator();
        while(it.hasNext()){
            h.add(it.next());
        }
        this.historico = h;
    }
    
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass()))return false;
        Cliente a = (Cliente) o;
        return (this.email.equals(a.getEmail()));
    }
    public String toString(){  
        StringBuilder s = new StringBuilder(); 
        s.append("Email: ");
        s.append(email);
        s.append(System.lineSeparator());
        s.append("Nome: ");
        s.append(nome);
        s.append(System.lineSeparator());
        s.append("Morada: ");
        s.append(morada);
        s.append(System.lineSeparator());
        s.append("Data de nascimento: ");
        SimpleDateFormat aux= new SimpleDateFormat("dd/MM/yyyy");
        String auxS=aux.format(dataNascimento.getTime());
        s.append(auxS.toString());
        s.append(System.lineSeparator());
        return s.toString();
    }
    public Cliente clone(){return new Cliente(this);}
    
    public void addAluguer(Aluguer a){
        this.historico.add(a.clone());
    }
}

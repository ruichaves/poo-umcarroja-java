import java.io.*;
import java.util.*;

public class CarroElectrico extends Veiculos{

    public CarroElectrico(String descricao,boolean estado,String matricula,int velocidade, double preco, int consumo, int classificacao, int autonomia, Coordenadas local){
        super(descricao,estado,matricula,velocidade,preco,consumo,classificacao,autonomia,local);
    }
    
    public CarroElectrico(CarroElectrico ce){
        super(ce);
    }
    
    public boolean equals(Object o){
        if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass()))return false;
        CarroElectrico a = (CarroElectrico) o;
        return (this.getMatricula().equals(a.getMatricula()));
    }
    
    public CarroElectrico clone(){
        return new CarroElectrico(this);
    }
}

import java.util.*;
import java.io.*;

public class RedeClientes{
    TreeMap<String,Cliente> rede;
    
    public RedeClientes(){
        rede = new TreeMap<String,Cliente>();
    }
    
    public RedeClientes(RedeClientes rc){
        this.rede = rc.getRede();
    }
    
    public TreeMap<String,Cliente> getRede(){
        TreeMap<String,Cliente> aux = new TreeMap<String,Cliente>();
        for(Cliente c:rede.values()){
            aux.put(c.getEmail(),c.clone());
        }
        return aux;
    }
    
    public void setRede(TreeMap<String,Cliente> rc){
        TreeMap<String,Cliente> aux = new TreeMap<String,Cliente>();
        for(Cliente c:rc.values()){
            aux.put(c.getEmail(),c.clone());
        }
        rede=aux;
    }
    
    public void addCliente(Cliente c){
        rede.put(c.getEmail(),c.clone());
    }
    
    public void removeCliente (Cliente c){
        rede.remove(c.getEmail());
    }
    
    public int login(String email,String password){
        if (rede.get(email).getPass().equals(password)) return 1;
        else return 0;
    }
    
    public Cliente getCliente(String c){
        return rede.get(c).clone();
    }
    
    public void trocaCliente(Cliente c){
        rede.put(c.getEmail(),c.clone());
    }
    
    public RedeClientes clone(){
        return new RedeClientes(this);
    }
    
    public boolean equals(Object o){
        if (o == this) return true;
        if ((o==null) || (this.getClass() != o.getClass())) return false;
        RedeClientes aux = (RedeClientes) o;
        int conta = 0;
        for (Cliente c: rede.values()){
            if (c.equals(aux.getCliente(c.getEmail()))) conta++;
        }
        if (conta == this.rede.size()) return true;
        return false;
    }
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        for(Cliente c: this.rede.values()) s.append(c.toString());
        s.append(System.lineSeparator());
        return s.toString();
    }
}

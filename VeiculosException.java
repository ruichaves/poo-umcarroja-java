import java.util.*;
import java.io.*;

public class VeiculosException extends Exception{
    public VeiculosException(){
        super();
    }
    
    public VeiculosException(String s){
        super(s);
    }
}

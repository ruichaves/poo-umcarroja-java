import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Menu{
    private List<String> opcoes;
    private int op;
    
    public Menu(String[] opcoes) {
        this.opcoes = new ArrayList<String>();
        for (String op : opcoes){
            this.opcoes.add(op);
        }
        this.op = 0;
    }
    
    public void executa() {
        do {
            mostrar();
            this.op = ler();
        } while (this.op == -1);
    }
    
    private void mostrar() {
        for (int i=0; i<this.opcoes.size(); i++) {
            System.out.print(i+1);
            System.out.print(" - ");
            System.out.println(this.opcoes.get(i));
        }
        System.out.println("0 - Sair");
    }
    
    private int ler(){
        int op;
        Scanner ler = new Scanner(System.in);
        System.out.print("Opção: ");
        op =ler.nextInt();
        if (op<0 || op>this.opcoes.size()) {
            System.out.println("Opção Inválida!");
            op = -1;
        }
        return op;
    }
    
    public int getOp (){
        return this.op;
    }
}

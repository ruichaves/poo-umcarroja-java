import java.util.*;
import java.io.*;

public class ActoresException extends Exception{
    public ActoresException(){
        super();
    }
    
    public ActoresException(String s){
        super(s);
    }
}

import java.io.*;
import java.util.*;
import java.text.*;

public class Coordenadas implements Serializable{
    private int x;
    private int y;
    
    public Coordenadas(){
        this.x = 0;
        this.y = 0;
    }
    public Coordenadas(int cx, int cy){
        this.x = cx;
        this.y = cy;
    }
    public Coordenadas(Coordenadas c){
        this.x=c.getX();
        this.x=c.getY();
    }
    
    public int getX(){return this.x;}
    public int getY(){return this.y;}
    public void setX(int cx){this.x=cx;}
    public void setY(int cy){this.y=cy;}
    
    public void movePonto(int cx,int cy){
        this.x=cx;
        this.y=cy;
    }
    
    public double distancia(Coordenadas local) {
        return Math.sqrt(Math.pow(this.x - local.getX(), 2) + Math.pow(this.y - local.getY(), 2));
    }
    
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    public boolean equals(Object o) {
        if (this == o) return true;
        if ((o == null) || (this.getClass() != o.getClass()))return false;
        Coordenadas c = (Coordenadas) o;
        return (this.x == c.getX() && this.y == c.getY());
    }
    
    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }
    
    public Coordenadas clone(){
        return new Coordenadas (this);
    }
}